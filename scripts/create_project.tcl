
# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir "."

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

variable script_file
set script_file "create_project.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "$script_file -tclargs \[--name <name>\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "\[--name\]               Project name"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
      "--help"       { help }
      "--name"       { incr i; set prj_name [lindex $::argv $i] }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
} else {
	puts ""
	puts "GK: NO PROJECT NAME SUPPLIED"
	puts ""
	return 1
}

puts ""
puts "GK: Creating project $prj_name"
puts ""

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/$prj_name"]"

# Create project
create_project $prj_name ./$prj_name -part xc7z045ffg900-3

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Set project properties
set obj [get_projects $prj_name]
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_output_repo" -value "$proj_dir$prj_name.cache/ip" -objects $obj
set_property -name "part" -value "xc7z045ffg900-3" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
set_property -name "xpm_libraries" -value "XPM_CDC XPM_FIFO" -objects $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set IP repository paths
set obj [get_filesets sources_1]
set_property "ip_repo_paths" "[file normalize "$origin_dir/../../"] [file normalize "$origin_dir/../../../DJPET_repo/sc_mod"]" $obj

# Rebuild user ip_repo's index before adding any source files
update_ip_catalog -rebuild

##    REPOS    ##

## GK add parent dir to repos and DJPET_repo
set_property  ip_repo_paths [list {$origin_dir/../../..} {$origin_dir/../../../../DJPET_repo/sc_mod}] [current_project]
update_ip_catalog


##    IP CORES    ##

## GK import IP Cores from ../ip directory to the local working directory
if {[file exists $origin_dir/../ip/] > 0} {
	set num_files [exec ls $origin_dir/../ip | wc -l]
	if {$num_files > 0} {
		foreach x [glob $origin_dir/../ip/*.xci] {import_ip $x}
	} else {
		puts ""
		puts "GK: no XCI files to import"
		puts ""
	}
} else {
	puts ""
	puts "GK: no XCI files to import"
	puts ""
}


##    BLOCK DESIGNS    ##

## GK import block design files 
if {[file exists $origin_dir/../bd/] > 0} {	
	set num_files [exec ls $origin_dir/../bd | wc -l]
	if {$num_files > 0} {
		foreach x [glob $origin_dir/../bd/*.bd] { add_files [make_wrapper [import_files $x] -top] }
	} else {
		puts ""
		puts "GK: no BD files to import"
		puts ""
	}
} else {
	puts ""
	puts "GK: no BD files to import"
	puts ""
}

##    HDL SOURCES    ##

## GK adding hdl sources
# Set 'sources_1' fileset object
set obj [get_filesets sources_1]

if {[file exists $origin_dir/../hdl/] > 0} {
	set num_files [exec ls $origin_dir/../hdl | wc -l]
	if {$num_files > 0} {
		foreach x [glob $origin_dir/../hdl/*.vh*] {add_files -norecurse -fileset $obj $x}
	} else {
		puts ""
		puts "GK: no HDL files to import"
		puts ""
	}
} else {
	puts ""
	puts "GK: no HDL files to import"
	puts ""
}

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property -name "top" -value "main" -objects $obj


##    XDC    ##

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

if {[file exists $origin_dir/../xdc/] > 0} {
	set num_files [exec ls $origin_dir/../xdc | wc -l]
	if {$num_files > 0} {
		foreach x [glob $origin_dir/../xdc/*.xdc] {
			add_files -norecurse -fileset $obj $x
		}
		
	} else {
		puts ""
		puts "GK: no XDC files to import"
		puts ""
	}
} else {
	puts ""
	puts "GK: no XDC files to import"
	puts ""
}






# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
# Empty (no sources present)

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property -name "top" -value "main" -objects $obj

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part xc7z045ffg900-3 -flow {Vivado Synthesis 2017} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
  set_property flow "Vivado Synthesis 2017" [get_runs synth_1]
}
set obj [get_runs synth_1]
set_property -name "part" -value "xc7z045ffg900-3" -objects $obj

# set the current synth run
current_run -synthesis [get_runs synth_1]

# Create 'impl_1' run (if not found)
if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part xc7z045ffg900-3 -flow {Vivado Implementation 2017} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
} else {
  set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
  set_property flow "Vivado Implementation 2017" [get_runs impl_1]
}
set obj [get_runs impl_1]
set_property -name "part" -value "xc7z045ffg900-3" -objects $obj
set_property -name "steps.write_bitstream.args.readback_file" -value "0" -objects $obj
set_property -name "steps.write_bitstream.args.verbose" -value "0" -objects $obj

# set the current impl run
current_run -implementation [get_runs impl_1]

puts "INFO: Project created:$prj_name"


## GK create slave control endpoint
#puts "INFO: GK: creating slave control endpoint"
#source $origin_dir/../../../DJPET_repo/sc_mod/ip_repo/scripts/sc_endpoint_gen.tcl
#jpet::sc_endpoint_gen -reg_qty 255
