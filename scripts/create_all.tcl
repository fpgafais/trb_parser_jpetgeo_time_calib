
# the scripts run standard generic create_project.tcl script and then runs project specific commands
# 
# args: --name <project>

global argv
global argc

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
      "--help"       { help }
      "--name"       { incr i; set prj_name [lindex $::argv $i] }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
} else {
	puts ""
	puts "GK: --name argument has to supplied"
	puts ""
	return 1
}

set prj_name "--name $prj_name"

set argv $prj_name
set argc 2

source ../scripts/create_project.tcl

### custom commands start here:


# set parser_wrapper as top module instead of standard main
set obj [get_filesets sources_1]
set_property -name "top" -value "parser_wrapper" -objects $obj
set obj [get_filesets sim_1]
set_property -name "top" -value "parser_wrapper" -objects $obj


## GK build ip from current

set version "_v1_0"

ipx::package_project -root_dir "$origin_dir/.." -vendor user.org -library user -taxonomy /UserIP
set_property name $prj_name [ipx::current_core]
set_property display_name $prj_name$version [ipx::current_core]
set_property description $prj_name$version [ipx::current_core]
set rev [get_property core_revision [ipx::current_core]]
incr rev
set_property core_revision $rev [ipx::current_core]
set_property supported_families {zynq Production zynquplus Beta} [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
