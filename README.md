## DESCRIPTION

TRB data parser including JPet geometry mapping, time calculations together with time JPet time offsets corrections.

## REQUIREMENTS

Requires GetJpetLocationCalib HLS module.

## COMPILATON

Create and enter ./work directory and run:

```
vivado -mode batch -source ../scripts/create_all.tcl -tclargs --name trb_parser_jpetgeo_time_calib
```

## NOTES

* The compile tcl script automatically adds the parent directory to the IP Cores repository, looking for GetJpetLocationCalib

* The compile tcl script automatically generates IP Core at the end
