-- This source file was created for J-PET project in WFAIS (Jagiellonian University in Cracow)
-- License for distribution outside WFAIS UJ and J-PET project is GPL v 3
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TDC_parser is
	port(
	    CLK : in std_logic;
		DATA_VALID_IN : in std_logic;
		DATA_IN : in std_logic_vector(31 downto 0);
		CHANNEL_OFFSET_IN : in std_logic_vector(15 downto 0);
		
		TIME_ISRISING_OUT : out std_logic;
		TIME_CHANNEL_OUT : out std_logic_vector(15 downto 0);
		TIME_CHANNEL_IS_REF_OUT : out std_logic;
		TIME_EPOCH_OUT : out std_logic_vector(27 downto 0);
		TIME_FINE_OUT: out std_logic_vector(9 downto 0);
		TIME_COARSE_OUT : out std_logic_vector(10 downto 0);
		TIME_VALID_OUT : out std_logic 
	);
end TDC_parser;

architecture Behavioral of TDC_parser is

    signal data_valid_l : std_logic := '0';
    signal data_l : std_logic_vector(31 downto 0) := x"0000_0000";
    signal channel_offset_l : std_logic_vector(15 downto 0) := x"0000";
    signal event_ok : std_logic := '0';
    signal current_epoch : std_logic_vector(27 downto 0) := x"000_0000";
    signal fine : std_logic_vector(9 downto 0) := b"00_0000_0000";
    signal coarse : std_logic_vector(10 downto 0) := b"000_0000_0000";
    signal channel : std_logic_vector(6 downto 0) := b"000_0000";
    signal rising : std_logic := '0';
    signal hit_found : std_logic := '0';
    signal channel_is_ref : std_logic := '0';
    
begin

    process(CLK)
    begin
        if rising_edge(CLK) then
            data_valid_l        <= DATA_VALID_IN;
            data_l              <= DATA_IN;
            
            TIME_FINE_OUT       <= fine;
            TIME_COARSE_OUT     <= coarse;
            TIME_EPOCH_OUT      <= current_epoch;
            TIME_CHANNEL_OUT    <= channel + channel_offset_l;
            TIME_CHANNEL_IS_REF_OUT <= channel_is_ref;
            TIME_ISRISING_OUT   <= rising;
            TIME_VALID_OUT      <= hit_found;
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            -- first word
            --if (data_valid_l = '0' and DATA_VALID_IN = '1') then
                channel_offset_l <= CHANNEL_OFFSET_IN;
            --else
            --    channel_offset_l <= channel_offset_l;
            --end if;
        end if;
    end process;   
    
    process(CLK)
    begin
        if rising_edge(CLK) then
        
            current_epoch   <= current_epoch;
            event_ok        <= event_ok;
            fine            <= (others => '0');
            coarse          <= (others => '0');
            channel         <= (others => '0');
            rising          <= '0';
            hit_found       <= '0';
            channel_is_ref  <= '0';
                   
            if (data_valid_l = '1') then
                
                case data_l(31 downto 29) is
                
                    -- tdc header 
                    when "001" =>
                        if (data_l(15 downto 1) = "000" & x"000") then
                            event_ok <= '1';
                        else
                            event_ok <= '0';
                        end if;
                    
                    -- tdc epoch counter
                    when "011" =>
                        current_epoch <= data_l(27 downto 0);
                        
                    -- tdc time data
                    when "100" =>
                        fine            <= data_l(21 downto 12);
                        coarse          <= data_l(10 downto 0);
                        channel         <= data_l(28 downto 22);
                        rising          <= data_l(11);          
                        
                        if (data_l(21 downto 12) = x"3ff") then
                            hit_found       <= '0';
                        else
                            hit_found       <= '1';
                        end if;
                        
                        if (data_l(28 downto 22) = "0000000") then
                            channel_is_ref <= '1';
                        else
                            channel_is_ref <= '0';
                        end if;
                        
                    when others => null;
                
                end case;
                
            end if;
        end if;
    end process; 

end Behavioral;
