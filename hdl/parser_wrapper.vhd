----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/14/2015 11:51:23 AM
-- Design Name: 
-- Module Name: parser_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity parser_wrapper is
    generic (
            SIMULATE              : integer range 0 to 1   := 0;
            INCLUDE_DEBUG         : integer range 0 to 1   := 0;
            MEM_SIZE              : integer range 0 to 255 := 255;
            TDC_PREFIX              : std_logic_vector(15 downto 0) := x"e000"
    );
    Port ( CLK   : in STD_LOGIC;
           RESET : in STD_LOGIC;
           
           -- data from gbe/udp receiver
           USR_DATA_IN       : in STD_LOGIC_VECTOR (31 downto 0);
           USR_DATA_VALID_IN : in STD_LOGIC;
           USR_DATA_SOP_IN   : in STD_LOGIC;
           USR_DATA_EOP_IN   : in STD_LOGIC;
           
           -- configuration for device id filter
           FLTR_CONF_ENDP_ADDR_IN   : in std_logic_vector(15 downto 0);
           FLTR_CONF_ENDP_OFFSET_IN : in std_logic_vector(15 downto 0);
           FLTR_CONF_ENDP_WE_IN     : in std_logic;
           
           -- parser output
           PSR_HIT_VALID_OUT : out std_logic;
           PSR_CHANNEL_OUT   : out std_logic_vector(15 downto 0);
           PSR_CHANNEL_IS_REF_OUT : out std_logic;
           PSR_ISRISING_OUT  : out std_logic;
           PSR_EPOCH_OUT     : out std_logic_vector(27 downto 0);
           PSR_FINE_OUT      : out std_logic_vector(9 downto 0);
           PSR_COARSE_OUT    : out std_logic_vector(10 downto 0);
           PSR_TRIGGERNR_OUT : out std_logic_vector(31 downto 0);
           PSR_TRIGGERNR_VALID_OUT : out std_logic;
           GEO_SIDE_OUT : out std_logic;
           GEO_SLOT_OUT : out std_logic_vector(7 downto 0);
           GEO_LAYER_OUT : out std_logic_vector(1 downto 0);
           GEO_THRESHOLD_OUT : out std_logic_vector(1 downto 0);
           TIME_RELATIVE_OUT : out std_logic_vector(31 downto 0);
           TIME_BIN_OUT : out std_logic_vector(7 downto 0);
           
           TIME_GLOBAL_REF_OUT : out std_logic_vector(28 + 11 - 1 downto 0);
           TIME_GLOBAL_REF_IN : in std_logic_vector(28 + 11 - 1 downto 0);
           TIME_FULL_OUT : out std_logic_vector(20 downto 0);
           
           DEBUG_OUT         : out STD_LOGIC_VECTOR (7 downto 0)
    );
end parser_wrapper;

architecture Behavioral of parser_wrapper is

    COMPONENT GetJpetLocationCalib_0
    PORT (
      agg_result_slot_ap_vld : OUT STD_LOGIC;
      agg_result_layer_ap_vld : OUT STD_LOGIC;
      agg_result_side_ap_vld : OUT STD_LOGIC;
      agg_result_threshold_ap_vld : OUT STD_LOGIC;
      agg_result_calib_ap_vld : OUT STD_LOGIC;
      ap_clk : IN STD_LOGIC;
      ap_rst : IN STD_LOGIC;
      ap_start : IN STD_LOGIC;
      ap_done : OUT STD_LOGIC;
      ap_idle : OUT STD_LOGIC;
      ap_ready : OUT STD_LOGIC;
      agg_result_slot : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      agg_result_layer : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      agg_result_side : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      agg_result_threshold : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      agg_result_calib : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      tdc_channel : IN STD_LOGIC_VECTOR(15 DOWNTO 0)
    );
    END COMPONENT;
    
    component trb_ref_time_calc is
    Port (
        CLK_IN : in std_logic;
        RESET_IN : in std_logic;
    
        PSR_SOP_IN : in std_logic;
        PSR_HIT_VALID_IN : in std_logic;
        PSR_CHANNEL_IN   : in std_logic_vector(15 downto 0);
        PSR_CHANNEL_IS_REF_IN : in std_logic;
        PSR_ISRISING_IN  : in std_logic;
        PSR_EPOCH_IN     : in std_logic_vector(27 downto 0);
        PSR_FINE_IN      : in std_logic_vector(9 downto 0);
        PSR_COARSE_IN    : in std_logic_vector(10 downto 0);
            
        TIME_CALIB_IN    : in std_logic_vector(15 downto 0);
        
        TIME_RELATIVE_OUT : out std_logic_vector(31 downto 0);
        TIME_BIN_OUT : out std_logic_vector(7 downto 0);
        TIME_VALID_OUT : out std_logic;
        
        TIME_GLOBAL_REF_OUT : out std_logic_vector(28 + 11 - 1 downto 0);
        TIME_GLOBAL_REF_IN : in std_logic_vector(28 + 11 - 1 downto 0);
        TIME_FULL_OUT : out std_logic_vector(20 downto 0)
     );
    end component;

    signal parser_eventID : std_logic_vector(31 downto 0);
    signal parser_triggerID : std_logic_vector(31 downto 0);
    signal parser_deviceID : std_logic_vector(15 downto 0);
    signal parser_dataWORD : std_logic_vector(31 downto 0);
    signal parser_data_valid : std_logic;
    signal tdc_data_valid : std_logic;
    
    signal device_filter_channel_offset : std_logic_vector(15 downto 0);
    signal device_filter_accepted : std_logic;
    signal debug_leds : std_logic_vector(7 downto 0);
    
    signal ap_start : std_logic;
    
    signal psr_channel, psr_channel_q, psr_channel_qq, psr_channel_qqq : std_logic_vector(15 downto 0);
    signal psr_epoch, psr_epoch_q, psr_epoch_qq, psr_epoch_qqq : std_logic_vector(27 downto 0);
    signal psr_coarse, psr_coarse_q, psr_coarse_qq, psr_coarse_qqq : std_logic_vector(10 downto 0);
    signal psr_fine, psr_fine_q, psr_fine_qq, psr_fine_qqq : std_logic_vector(9 downto 0);
    signal psr_isrising, psr_isrising_q, psr_isrising_qq, psr_isrising_qqq, psr_hit, psr_hit_q, psr_hit_qq, psr_hit_qqq : std_logic;
    signal psr_channel_is_ref, psr_channel_is_ref_q, psr_channel_is_ref_qq, psr_channel_is_ref_qqq : std_logic;
    
    signal geo_side, geo_slot, geo_layer, geo_threshold, geo_threshold_o : std_logic_vector(15 downto 0);
    signal geo_side_q, geo_slot_q, geo_layer_q, geo_threshold_q, geo_threshold_o_q : std_logic_vector(15 downto 0);
    signal geo_side_qq, geo_slot_qq, geo_layer_qq, geo_threshold_qq, geo_threshold_o_qq : std_logic_vector(15 downto 0);
    signal parser_triggerid_valid : std_logic;
    
    signal calib : std_logic_vector(15 downto 0);
    
begin


    trb_parser_inst : entity work.trb_parser
    generic map(
        SIMULATE              => SIMULATE,
        INCLUDE_DEBUG         => INCLUDE_DEBUG
    )
    port map (
        CLK                => CLK,
        RESET              => RESET,
        
        USR_DATA_SOP_IN    => USR_DATA_SOP_IN,
        USR_DATA_EOP_IN    => USR_DATA_EOP_IN,
        USR_DATA_VALID_IN  => USR_DATA_VALID_IN,
        USR_DATA_IN        => USR_DATA_IN,

        PSR_EVENT_ID_OUT   => parser_eventid,
        PSR_TRIGGER_ID_OUT => parser_triggerid,
        PSR_TRIGGERNR_VALID_OUT => parser_triggerid_valid,
        PSR_DEVICE_ID_OUT  => parser_deviceid,
        PSR_DATA_OUT       => parser_dataword,
        PSR_DATA_VALID_OUT => parser_data_valid
    );

    device_filter : entity work.endpoint_filter2
	generic map(
		SIMULATE              => SIMULATE,
		INCLUDE_DEBUG         => INCLUDE_DEBUG,

		LATTICE_ECP3          => 0,
		XILINX_SERIES7_ISE    => 0,
		XILINX_SERIES7_VIVADO => 1,

		MEM_SIZE              => MEM_SIZE,
		TDC_PREFIX            => TDC_PREFIX
	)
	port map(
		CLK                    => CLK,
		RESET                  => RESET,

		-- config interface
		CONFIG_ENDP_ADDR_IN    => FLTR_CONF_ENDP_ADDR_IN,
		CONFIG_ENDP_OFFSET_IN  => FLTR_CONF_ENDP_OFFSET_IN,
		CONFIG_WE_IN           => FLTR_CONF_ENDP_WE_IN,

		-- user interface
		FILTER_ENDP_ADDR_IN    => parser_deviceID,
		FILTER_ENDP_OFFSET_OUT => device_filter_channel_offset,
		FILTER_ENDP_VALID_OUT  => device_filter_accepted,

		DEBUG_OUT              => open
	);
	
	tdc_data_valid <= device_filter_accepted and parser_data_valid;


    TdcParser : entity work.tdc_parser 
    port map (
        CLK                 => CLK,
        DATA_VALID_IN       => tdc_data_valid,
        DATA_IN             => parser_dataWORD,
        CHANNEL_OFFSET_IN   => device_filter_channel_offset,
        
        TIME_ISRISING_OUT   => psr_isrising,
        TIME_CHANNEL_OUT    => psr_channel,
        TIME_CHANNEL_IS_REF_OUT => psr_channel_is_ref,
        TIME_EPOCH_OUT      => psr_epoch,
        TIME_FINE_OUT       => psr_fine,
        TIME_COARSE_OUT     => psr_coarse,
        TIME_VALID_OUT      => psr_hit 
    );
    
    jpet_geo_inst : GetJpetLocationCalib_0
      PORT MAP (
        agg_result_slot_ap_vld => open,
        agg_result_layer_ap_vld => open,
        agg_result_side_ap_vld => open,
        agg_result_threshold_ap_vld => open,
        agg_result_calib_ap_vld => open,
        ap_clk => CLK,
        ap_rst => RESET,
        ap_start => ap_start,
        ap_done => open,
        ap_idle => open,
        ap_ready => open,
        agg_result_slot => geo_slot,
        agg_result_layer => geo_layer,
        agg_result_side => geo_side,
        agg_result_threshold => geo_threshold,
        agg_result_calib => calib,
        tdc_channel => psr_channel
      );
      
      time_inst : trb_ref_time_calc
      Port map(
          CLK_IN => CLK,
          RESET_IN => RESET,
      
          PSR_SOP_IN       => USR_DATA_SOP_IN,
          PSR_HIT_VALID_IN => psr_hit,
          PSR_CHANNEL_IN   => psr_channel,
          PSR_CHANNEL_IS_REF_IN => psr_channel_is_ref,
          PSR_ISRISING_IN  => psr_isrising,
          PSR_EPOCH_IN     => psr_epoch,
          PSR_FINE_IN      => psr_fine,
          PSR_COARSE_IN    => psr_coarse,
          
          TIME_CALIB_IN    => calib,
          
          TIME_RELATIVE_OUT => TIME_RELATIVE_OUT,
          TIME_BIN_OUT => TIME_BIN_OUT,
          TIME_VALID_OUT => PSR_HIT_VALID_OUT,
          
          TIME_GLOBAL_REF_OUT => TIME_GLOBAL_REF_OUT,
          TIME_GLOBAL_REF_IN => TIME_GLOBAL_REF_IN,
          TIME_FULL_OUT => TIME_FULL_OUT 
       );
      
      ap_start <= '1' when psr_hit = '1' and psr_isrising = '1' else '0';
      
      geo_threshold_o <= geo_threshold - x"1";
      
      process(CLK)
      begin
        if rising_edge(CLK) then
        
            psr_hit_q <= psr_hit;
            psr_channel_q <= psr_channel;
            psr_isrising_q <= psr_isrising;
            psr_epoch_q <= psr_epoch;
            psr_fine_q <= psr_fine;
            psr_coarse_q <= psr_coarse;
            psr_channel_is_ref_q <= psr_channel_is_ref;
            
            psr_hit_qq <= psr_hit_q;
            psr_channel_qq <= psr_channel_q;
            psr_isrising_qq <= psr_isrising_q;
            psr_epoch_qq <= psr_epoch_q;
            psr_fine_qq <= psr_fine_q;
            psr_coarse_qq <= psr_coarse_q;
            psr_channel_is_ref_qq <= psr_channel_is_ref_q;
            
            psr_hit_qqq <= psr_hit_qq;
            psr_channel_qqq <= psr_channel_qq;
            psr_isrising_qqq <= psr_isrising_qq;
            psr_epoch_qqq <= psr_epoch_qq;
            psr_fine_qqq <= psr_fine_qq;
            psr_coarse_qqq <= psr_coarse_qq;
            psr_channel_is_ref_qqq <= psr_channel_is_ref_qq;
            
            geo_side_q <= geo_side;
            geo_slot_q <= geo_slot;
            geo_layer_q <= geo_layer;
            geo_threshold_o_q <= geo_threshold_o;
            
            geo_side_qq <= geo_side_q;
            geo_slot_qq <= geo_slot_q;
            geo_layer_qq <= geo_layer_q;
            geo_threshold_o_qq <= geo_threshold_o_q;
        
            --PSR_HIT_VALID_OUT <= psr_hit_qq;
            PSR_CHANNEL_OUT   <= psr_channel_qqq;
            PSR_CHANNEL_IS_REF_OUT <= psr_channel_is_ref_qqq;
            PSR_ISRISING_OUT  <= psr_isrising_qqq;
            PSR_EPOCH_OUT     <= psr_epoch_qqq;
            PSR_FINE_OUT      <= psr_fine_qqq;
            PSR_COARSE_OUT    <= psr_coarse_qqq;
            PSR_TRIGGERNR_OUT <= parser_triggerid;
            PSR_TRIGGERNR_VALID_OUT <= parser_triggerid_valid;
            GEO_SIDE_OUT      <= geo_side_qq(0);
            GEO_SLOT_OUT      <= geo_slot_qq(7 downto 0);
            GEO_LAYER_OUT     <= geo_layer_qq(1 downto 0);
            GEO_THRESHOLD_OUT <= geo_threshold_o_qq(1 downto 0);
        end if;
      end process;

end Behavioral;
