----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.05.2016 14:00:28
-- Design Name: 
-- Module Name: trb_parser - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity trb_parser is
    generic (
        SIMULATE              : integer range 0 to 1   := 0;
        INCLUDE_DEBUG         : integer range 0 to 1   := 0
    );
    port (
            CLK : in  STD_LOGIC;
            RESET : in  STD_LOGIC;
            USR_DATA_SOP_IN : in  STD_LOGIC;
            USR_DATA_EOP_IN : in  STD_LOGIC;
            USR_DATA_VALID_IN : in  STD_LOGIC;
            USR_DATA_IN : in STD_LOGIC_VECTOR(31 downto 0);
    
            PSR_EVENT_ID_OUT : out std_logic_vector(31 downto 0);
            PSR_TRIGGER_ID_OUT : out std_logic_vector(31 downto 0);
            PSR_TRIGGERNR_VALID_OUT : out std_logic;
            PSR_DEVICE_ID_OUT : out std_logic_vector(15 downto 0);
            PSR_DATA_OUT : out std_logic_vector(31 downto 0);
            PSR_DATA_VALID_OUT : out std_logic
    );
end trb_parser;

architecture Behavioral of trb_parser is

    type parser_states  is (IDLE, QUEUE_HEADER, SUB_HEADER, SUB_DATA, CLEANUP);
    signal parser_current_state, parser_next_state : parser_states;
    
    signal data_ctr, subsub_data_ctr : natural range 0 to 65535 := 0;
    signal queue_size, sub_size, subsub_size, device_id : std_logic_vector(15 downto 0);
    signal current_data_word : std_logic_vector(31 downto 0);
    signal data_word_valid : std_logic;
    signal trigger_nr : std_logic_vector(31 downto 0);
    signal trigger_nr_valid : std_logic;

begin

    process(CLK)
    begin
        if rising_edge(CLK) then
            PSR_DEVICE_ID_OUT <= device_id;
            PSR_TRIGGER_ID_OUT <= trigger_nr;
            PSR_TRIGGERNR_VALID_OUT <= trigger_nr_valid;
        end if;
    end process;
    
    PSR_DATA_OUT       <= current_data_word;
    PSR_DATA_VALID_OUT <= data_word_valid;

    process(CLK)
    begin
        if rising_edge(CLK) then
            if (RESET = '1') then
                parser_current_state <= IDLE;
            else
                parser_current_state <= parser_next_state;
            end if;
        end if;
    end process;
    
    process(parser_current_state, USR_DATA_SOP_IN, data_ctr)
    begin
        case (parser_current_state) is
        
            when IDLE =>
                if (USR_DATA_SOP_IN = '1') then
                    parser_next_state <= QUEUE_HEADER;
                else
                    parser_next_state <= IDLE;
                end if;
            
            when QUEUE_HEADER =>
                if (data_ctr = 1) then
                    parser_next_state <= SUB_HEADER;
                else
                    parser_next_state <= QUEUE_HEADER;
                end if;                
            
            when SUB_HEADER =>
                if (data_ctr = 3) then
                    parser_next_state <= SUB_DATA;
                else
                    parser_next_state <= SUB_HEADER;
                end if; 
            
            when SUB_DATA =>
                if (data_ctr + 20 = sub_size) then
                    parser_next_state <= CLEANUP;
                else
                    parser_next_state <= SUB_DATA;
                end if;
            
            when CLEANUP => parser_next_state <= IDLE;
        
        end case;
    end process;
    
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            if (parser_current_state = IDLE and USR_DATA_VALID_IN = '0') then
                data_ctr <= 0;
            elsif (parser_current_state = IDLE and USR_DATA_VALID_IN = '1') then
                data_ctr <= 1;
            elsif (parser_current_state = QUEUE_HEADER and USR_DATA_VALID_IN = '1') then
                if (data_ctr = 1) then
                    data_ctr <= 0;
                else
                    data_ctr <= data_ctr + 1;
                end if;
            elsif (parser_current_state = SUB_HEADER and USR_DATA_VALID_IN = '1') then
                if (data_ctr = 3) then
                    data_ctr <= 0;
                else
                    data_ctr <= data_ctr + 1;
                end if;
            elsif (parser_current_state = SUB_DATA and USR_DATA_VALID_IN = '1') then
                data_ctr <= data_ctr + 4;
            else
                data_ctr <= data_ctr;
            end if;
        end if;
    end process;
        
    process(CLK)
    begin
        if rising_edge(CLK) then
        
            queue_size <= queue_size;
            
            if (parser_current_state = IDLE and USR_DATA_VALID_IN = '0') then
                queue_size <= (others => '0');
            elsif (parser_current_state = IDLE and USR_DATA_VALID_IN = '1' and data_ctr = 0) then
                queue_size <= USR_DATA_IN(15 downto 0);
            end if;
            
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            
            sub_size <= sub_size;
        
            if (parser_current_state = IDLE) then
                sub_size <= (others => '0');
            elsif (parser_current_state = SUB_HEADER and USR_DATA_VALID_IN = '1' and data_ctr = 0) then
                sub_size <= USR_DATA_IN(15 downto 0);
            end if;
            
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            
            trigger_nr <= trigger_nr;
            trigger_nr_valid <= '0';
        
            if (parser_current_state = IDLE) then
                trigger_nr <= (others => '0');
            elsif (parser_current_state = SUB_HEADER and USR_DATA_VALID_IN = '1' and data_ctr = 3) then
                trigger_nr <= USR_DATA_IN;
                trigger_nr_valid <= '1';
            end if;
                        
        end if;
    end process;
    
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            
            subsub_data_ctr <= subsub_data_ctr;
            
            if (parser_current_state = IDLE) then
                subsub_data_ctr <= 0;
            elsif (parser_current_state = SUB_DATA and USR_DATA_VALID_IN = '1') then
                if (subsub_data_ctr = subsub_size and subsub_size /= x"0000") then
                    subsub_data_ctr <= 0;
                else
                    subsub_data_ctr <= subsub_data_ctr + 1;
                end if;
            end if;
        end if;
    end process;    
    
    process(CLK)
    begin
        if rising_edge(CLK) then
        
            subsub_size <= subsub_size;
            
            if (parser_current_state = IDLE) then
                subsub_size <= (others => '0');
            elsif (parser_current_state = SUB_DATA and USR_DATA_VALID_IN = '1' and subsub_data_ctr = 0) then
                subsub_size(15 downto 0) <= USR_DATA_IN(31 downto 16);
            end if;
        
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
            
            device_id <= device_id;
            
            if (parser_current_state = IDLE) then
                device_id <= (others => '0');
            elsif (parser_current_state = SUB_DATA and USR_DATA_VALID_IN = '1' and subsub_data_ctr = 0) then
                device_id <= USR_DATA_IN(15 downto 0);
            end if;
            
        end if;
    end process;
    
    process(CLK)
    begin
        if rising_edge(CLK) then
        
            current_data_word <= current_data_word;
            data_word_valid <= '0';
        
            if (parser_current_state = SUB_DATA and USR_DATA_VALID_IN = '1') then
                current_data_word <= USR_DATA_IN;
                data_word_valid <= '1';
            end if;
            
        end if;
    end process;
        

end Behavioral;
