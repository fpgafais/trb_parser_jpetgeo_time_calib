library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_signed.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trb_ref_time_calc is
Port (
    CLK_IN : in std_logic;
    RESET_IN : in std_logic;

    PSR_SOP_IN : in std_logic;
    PSR_HIT_VALID_IN : in std_logic;
    PSR_CHANNEL_IN   : in std_logic_vector(15 downto 0);
    PSR_CHANNEL_IS_REF_IN : in std_logic;
    PSR_ISRISING_IN  : in std_logic;
    PSR_EPOCH_IN     : in std_logic_vector(27 downto 0);
    PSR_FINE_IN      : in std_logic_vector(9 downto 0);
    PSR_COARSE_IN    : in std_logic_vector(10 downto 0);
    
    TIME_CALIB_IN    : in std_logic_vector(15 downto 0);
    
    TIME_RELATIVE_OUT : out std_logic_vector(31 downto 0);
    TIME_BIN_OUT : out std_logic_vector(7 downto 0);
    TIME_VALID_OUT : out std_logic;
    
    TIME_GLOBAL_REF_OUT : out std_logic_vector(28 + 11 - 1 downto 0);
    TIME_GLOBAL_REF_IN : in std_logic_vector(28 + 11 - 1 downto 0);
    
    TIME_FULL_OUT : out std_logic_vector(20 downto 0) 
 );
end trb_ref_time_calc;

architecture Behavioral of trb_ref_time_calc is

    signal ref, ref_global : std_logic_vector(28 + 11 - 1 downto 0) := (others => '0');
    signal lead : std_logic_vector(28 + 11 - 1 downto 0) := (others => '0');
    signal time_rel : std_logic_vector(28 + 11 - 1 downto 0) := (others => '0');
    signal hit_valid_q, hit_valid_qq : std_logic;
    
    signal ref_signed, global_ref_signed, correction_signed, hit_rel_signed, hit_signed : signed(28 + 11 downto 0);
    signal hit_rel : std_logic_vector(39 downto 0);
    
    signal ref_full_s, ref_global_full_s, lead_full_s, hit_rel_full_s : signed(56 downto 0) := (others => '0');
    signal correction_full_s : signed(56 downto 0);

    signal epoch : unsigned(55 downto 0) := (others => '0');
    signal coarse : unsigned(21 downto 0) := (others => '0');
    signal fine : unsigned(9 downto 0) := (others => '0');

    signal is_ref, valid : std_logic;
    signal channel, ch_zeros : std_logic_vector(6 downto 0);
    
    --signal global_ref_shft, global_ref_shft_q : std_logic_vector(3 downto 0);
    
    signal first_ref : std_logic;
    
    signal calib : unsigned(15 downto 0);
    signal calib_s : signed(16 downto 0);
    

begin

    ch_zeros <= (others => '0');

    process(CLK_IN)
    begin
        if rising_edge(CLK_IN) then

            epoch <= unsigned(PSR_EPOCH_IN) * 1024000;
            coarse <= unsigned(PSR_COARSE_IN) * 500;
            fine <= unsigned(PSR_FINE_IN);
            
            calib <= unsigned(TIME_CALIB_IN);

            valid <= PSR_HIT_VALID_IN and PSR_ISRISING_IN;
            is_ref <= PSR_CHANNEL_IS_REF_IN;

            ref_global_full_s <= ref_global_full_s;
            ref_full_s <= ref_full_s;
            lead_full_s <= lead_full_s;
            
            if (valid = '1' and is_ref = '0') then
                hit_valid_q <= '1';
            else
                hit_valid_q <= '0';
            end if;

            if (valid = '1') then
                if (is_ref = '1') then
                    ref_full_s <= signed("0" & (epoch + coarse - fine));
                    
                    if (first_ref = '1') then
                        ref_global_full_s <= signed("0" & (epoch + coarse - fine));
                    end if;
                else
                    lead_full_s <= signed("0" & (epoch + coarse - fine));
                end if;
            end if;

        end if;
    end process;

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
        if (PSR_CHANNEL_IS_REF_IN = '1') then
            ref <= PSR_EPOCH_IN & PSR_COARSE_IN;
        else
            ref <= ref;
        end if;
    end if;
end process;

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
        if (PSR_SOP_IN = '1') then
            first_ref <= '1';
        elsif (is_ref = '1') then
            first_ref <= '0';
        else
            first_ref <= first_ref;
        end if;
    end if;
end process;

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
        if (PSR_CHANNEL_IS_REF_IN = '1' and first_ref = '1') then
            ref_global <= PSR_EPOCH_IN & PSR_COARSE_IN;
        else
            ref_global <= ref_global;
        end if;
    end if;
end process;

TIME_GLOBAL_REF_OUT <= ref_global;

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
        if (PSR_HIT_VALID_IN = '1' and PSR_ISRISING_IN = '1' and PSR_CHANNEL_IS_REF_IN = '0') then
            lead <= PSR_EPOCH_IN & PSR_COARSE_IN;
        else
            lead <= lead;
        end if;
        
        
        hit_valid_qq <= hit_valid_q;
    end if;
end process;

ref_signed <= signed("0" & ref);
global_ref_signed <= signed("0" & TIME_GLOBAL_REF_IN);
hit_signed <= signed("0" & lead);
hit_rel <= std_logic_vector(hit_rel_signed);

calib_s <= signed("0" & calib);

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
    
        correction_signed <= global_ref_signed - ref_signed;
        correction_full_s <= ref_global_full_s - ref_full_s;
    
        hit_rel_signed <=  ref_signed - hit_signed - correction_signed;
        hit_rel_full_s <= ref_full_s - lead_full_s - calib_s; -- - correction_full_s; -- - calib_s;
        
    end if;
end process;    

process(CLK_IN)
begin
    if rising_edge(CLK_IN) then
        TIME_VALID_OUT <= hit_valid_qq;
        TIME_RELATIVE_OUT <= hit_rel(31 downto 0);
        TIME_BIN_OUT <= hit_rel(11 downto 4);
        
        TIME_FULL_OUT <= std_logic_vector(hit_rel_full_s(20 downto 0));
    end if;
end process;

end Behavioral;
