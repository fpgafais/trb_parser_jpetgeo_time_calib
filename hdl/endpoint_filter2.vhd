-- This source file was created for J-PET project in WFAIS (Jagiellonian University in Cracow)
-- License for distribution outside WFAIS UJ and J-PET project is GPL v 3
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE IEEE.std_logic_UNSIGNED.ALL;


entity endpoint_filter2 is
	generic(
		SIMULATE              : integer range 0 to 1   := 0;
		INCLUDE_DEBUG         : integer range 0 to 1   := 0;

		LATTICE_ECP3          : integer range 0 to 1   := 0;
		XILINX_SERIES7_ISE    : integer range 0 to 1   := 0;
		XILINX_SERIES7_VIVADO : integer range 0 to 1   := 0;

		MEM_SIZE              : integer range 0 to 255 := 255; -- the number of possible endpoints
		TDC_PREFIX            : std_logic_vector(15 downto 0) := x"e000"
	);
	port(
		CLK                    : in  std_logic;
		RESET                  : in  std_logic;

		-- config interface
		CONFIG_ENDP_ADDR_IN    : in  std_logic_vector(15 downto 0);
		CONFIG_ENDP_OFFSET_IN  : in  std_logic_vector(15 downto 0);
		CONFIG_WE_IN           : in  std_logic;

		-- user interface
		FILTER_ENDP_ADDR_IN    : in  std_logic_vector(15 downto 0);
		FILTER_ENDP_OFFSET_OUT : out std_logic_vector(15 downto 0);
		FILTER_ENDP_VALID_OUT  : out std_logic;

		DEBUG_OUT              : out std_logic_vector(255 downto 0)
	);
end entity endpoint_filter2;

architecture RTL of endpoint_filter2 is
	type endp_array is array (0 to MEM_SIZE - 1) of std_logic_vector(16 downto 0);
	signal endpoints : endp_array;

	signal endp_addr_local, endp_offset_local : std_logic_vector(15 downto 0);
	signal endp_we_local                      : std_logic;
	signal filter_endp_addr                   : std_logic_vector(15 downto 0);

begin

    


	process(CLK)
	begin
		if rising_edge(CLK) then
			endp_addr_local   <= CONFIG_ENDP_ADDR_IN;
			endp_offset_local <= CONFIG_ENDP_OFFSET_IN;
			endp_we_local     <= CONFIG_WE_IN;
		end if;
	end process;
	
	filter_endp_addr  <= FILTER_ENDP_ADDR_IN - TDC_PREFIX when FILTER_ENDP_ADDR_IN(15 downto 8) = x"e0" else x"0000";


    regs_gen : for i in 0 to MEM_SIZE - 1 generate
        process(CLK)
        begin
            if rising_edge(CLK) then
                if (RESET = '1') then
                    endpoints(i) <= (others => '0');
                elsif (endp_we_local = '1' and i = to_integer(unsigned(endp_addr_local - TDC_PREFIX))) then
                    endpoints(i) <= "1" & endp_offset_local;
                else
                    endpoints(i) <= endpoints(i);
                end if;
            end if;
        end process;
    end generate regs_gen;
    
	process(CLK)
	begin
		if rising_edge(CLK) then
			FILTER_ENDP_OFFSET_OUT <= endpoints(to_integer(unsigned(filter_endp_addr)))(15 downto 0);
			FILTER_ENDP_VALID_OUT  <= endpoints(to_integer(unsigned(filter_endp_addr)))(16);
		end if;
	end process;

end architecture RTL;
